# Changelog
## 4.0.1 
- some cleaning for publishing

## 4.0.0 
- updates to Angular 4

## 2.0.0
- updated to Angular 2 

## 1.1.2
- hide label for select mode if empty

## 1.1.1

- changed p tag for radio group label to span

## 1.1.0

- Added label attribute
- Shortened current-value attribute to value
- Added display-style attribute
- Added mode to display this component as dom.SelectElement
- added random double number as name seed to option buttons name
  attributes to separate the options from one jb-option-input from another on the same page

## 1.0.0

- Initial version
