library app_component;

import 'package:angular/angular.dart';
import 'wochentage.dart';
import 'package:jb_option_input/jb_option_input.dart';

@Component(
    selector: "app-component",
    templateUrl: "app_component.html",
    directives: const [CORE_DIRECTIVES, JbOptionInput],
    exports: const [Wochentage])
class AppComponent {
  Map<String, dynamic> options =
      new Map.fromIterables(["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag"], Wochentage.values);

  Wochentage value1 = Wochentage.Montag;
  Wochentage value2 = Wochentage.Dienstag;
  Wochentage value3 = Wochentage.Mittwoch;

  AppComponent() {}
}
