# jb_option_input

An AngularDart 1 Component for being re-used in other AngularDart projects

## Usage
1. Add the component to your pubspec.yaml (see 'Install' tab)


2. Show your component to angular dependency injection 
   -> see App Class in app_initialize.dart

        import 'package:jb_option_input/jb_option_input.dart';

        class App extends Angular.Module {

          App() {
            bind(JbOptionInput);
          }
        }


## Additional Links

- <https://www.dartlang.org/tools/pub/dependencies.html>
- <http://simiansblog.com/2015/05/06/Using-Inquirer-js/>
- <https://github.com/SBoudrias/Inquirer.js/>
- <https://www.npmjs.com/package/change-case>
- <http://ejs.co/>
- <http://yeoman.io/authoring/user-interactions.html>
- <http://yeoman.io/authoring/>
- <https://github.com/yeoman/generator-generator>
