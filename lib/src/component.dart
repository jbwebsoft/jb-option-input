library jb_option_input.component;

import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:logging/logging.dart';
import 'package:logging_handlers/logging_handlers_shared.dart';
import 'dart:math';
import 'dart:async';
import 'package:quiver/collection.dart';

enum DisplayStyle { radio, verticalRadio, select }

@Component(
    selector: "jb-option-input[options][value]", directives: const [CORE_DIRECTIVES, formDirectives], templateUrl: "template.html")
class JbOptionInput {
  Logger _logger = new Logger("jb_option_input.component");

  String _chosenLabel;
  BiMap<String, dynamic> _options;
  Map<String, dynamic> sortedOptions;
  Completer optionsAvailable = new Completer();
  DisplayStyle _displayStyle = DisplayStyle.radio;

  //TODO: Maybe implement ControlValueAccessor interface later to be able to use this thing with ng-model
  //Example 1: https://blog.thoughtram.io/angular/2016/07/27/custom-form-controls-in-angular-2.html
  //Example 2: http://almerosteyn.com/2016/04/linkup-custom-control-to-ngcontrol-ngmodel

  @Input()
  set displayStyle(String value) {
    _displayStyle = DisplayStyle.values.firstWhere((style) => style.toString() == value);
  }

  String get displayStyle => _displayStyle.toString();

  ///Values are intended to be enum values
  @Input()
  set options(Map<String, dynamic> options) {
    sortedOptions = options;
    _options = new HashBiMap();
    _options.addAll(options);
    optionsAvailable.complete();
  }

  Map<String, dynamic> get options => _options;

  /// The initial value
  @Input()
  set value(initialValue) {
    optionsAvailable.future.then((_) {
      _chosenLabel = _options.inverse[initialValue];
    });
  }

  final StreamController _valueChange = new StreamController.broadcast();

  @Output()
  Stream get valueChange => _valueChange.stream;

  @Input()
  String label = "";

  String randomNameSeed = new Random().nextDouble().toString();

  get chosenLabel => _chosenLabel;

  set chosenLabel(String label) {
    _chosenLabel = label;
    _valueChange.add(_options[label]);
  }

  JbOptionInput() {
    hierarchicalLoggingEnabled = true;
    _logger.onRecord.listen(new LogPrintHandler());
    _logger.level = Level.ALL;
  }

  bool get displayAsRadio => _displayStyle == DisplayStyle.radio;

  bool get displayAsVerticalRadio => _displayStyle == DisplayStyle.verticalRadio;

  bool get displayAsSelect => _displayStyle == DisplayStyle.select;
}
